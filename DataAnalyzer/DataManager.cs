﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DataAnalyzer
{
    public class DataManager
    {
        private IDataRetrival _dataRetrival;
        private List<Person> _persons;
        private INotifier _notifier;

        public DataManager(IDataRetrival dataRetrival, INotifier notifier)
        {
            _dataRetrival = dataRetrival;
            _persons = _dataRetrival.GetAll();
            _notifier = notifier;
        }

        public List<Person> GetByFirstName(string name)
        {
            return _persons.FindAll(p => p.FirstName == name);
        }
        public List<Person> GetByLastName(string lastName)
        {
            return _persons.FindAll(p => p.LastName == lastName);
        }

        public List<Person> GetByDepartment(string department)
        {
            return _persons.FindAll(p => p.Department == department);
        }

        public Person GetById(int id)
        {
            var person = _persons.FirstOrDefault(p => p.Id == id);
            return person;
        }

        public int GetNumberOfVacationDays(int id) {
            var employee = GetById(id);
            var numberOfYearsInCompany = DateTime.Now.Year - employee.StartEmploymentDate.Year;

            if (numberOfYearsInCompany < 5)
            {
                return 23;
            }
            else if (numberOfYearsInCompany >= 5 && numberOfYearsInCompany < 10)
            {
                return 26;
            }
            else {
                return 29;
            }

        }

        public bool IsAllowedWorkFromHome(int id)
        {
            var employee = GetById(id);
            var numberOfYearsInCompany = DateTime.Now.Year - employee.StartEmploymentDate.Year;

            if (numberOfYearsInCompany < 2)
            {
                return false;
            }
            return true;
        }
        public void Notify(Person person, string message)
        {
            _notifier.SendNotification(person.Email, message);
        }

        public void NotifyDepartmant(string departmantName)
        {
            var persons = GetByDepartment(departmantName);
            _notifier.SendNotificationToGroup(persons, $"Notification for {departmantName}");
        }
    }
}
