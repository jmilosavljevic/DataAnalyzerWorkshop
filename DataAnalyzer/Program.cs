﻿using Autofac;
using System;

namespace DataAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
           // DataManager dataManager = new DataManager(new DataRetrivalFromFile(), new Notifier());
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<DataManager>();
            builder.RegisterType<Notifier>().As<INotifier>();
            builder.RegisterType<DataRetrivalFromFile>().As<IDataRetrival>();

            IContainer Container = builder.Build();

            var dataManager = Container.Resolve<DataManager>();

            var people = dataManager.GetByDepartment("Accounting");

            foreach (var person in people)
            {
                var message = $"{person.FirstName} {person.LastName} has {dataManager.GetNumberOfVacationDays(person.Id)} vacation days and " + (dataManager.IsAllowedWorkFromHome(person.Id) ? "is " : "in not ") + "allowed to work from home.";
                dataManager.Notify(person, message);
            }
        }
    }
}
