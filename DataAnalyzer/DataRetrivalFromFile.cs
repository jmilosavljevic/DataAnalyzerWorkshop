﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAnalyzer
{
    public class DataRetrivalFromFile : IDataRetrival
    {
        private string _fileName;
        private List<Person> _persons;

        public DataRetrivalFromFile()
        {
            _fileName = ConfigurationManager.AppSettings["fileName"];
            _persons = ReadFromFile();
        }

        public List<Person> GetAll() {
            return _persons;
        }
        public List<Person> ReadFromFile()
        {
            var persons = new List<Person>();
            using (var reader = new StreamReader(_fileName))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    var person = new Person
                    {
                        Id = Convert.ToInt32(values[0]),
                        FirstName = values[1],
                        LastName = values[2],
                        Email = values[3],
                        Department = values[4],
                        StartEmploymentDate = DateTime.Parse(values[5])
                    };
                    persons.Add(person);
                }
            }
            return persons;
        }


    }

    public interface IDataRetrival {
        List<Person> GetAll();
    }
}
