﻿using DataAnalyzer;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace DataAnalyzerUnitTest
{
    [TestFixture]
    public class DataManagerTests
    {
        private List<Person> employeeList;

        [SetUp]
        public void TestSetup()
        {
            employeeList = new List<Person>()
            {
                new Person{
                    Id=1,
                    FirstName="Ursa",
                    LastName="Maskell",
                    Department="Engineering",
                    Email = "umaskell1@apache.org",
                    StartEmploymentDate = new DateTime(2007, 7, 19)
                },
                  new Person{
                    Id=2,
                    FirstName="Agatha",
                    LastName="Joslin",
                    Department="Development",
                    Email = "ajoslin2@cloudflare.com",
                    StartEmploymentDate = new DateTime(2010, 7, 28)
                },
                    new Person{
                    Id=3,
                    FirstName="Nikolas",
                    LastName="Matuszynski",
                    Email="nmatuszynski4@devhub.com",
                    Department = "Development",
                    StartEmploymentDate = new DateTime(2006, 5, 25)
                },
                      new Person{
                    Id=4,
                    FirstName="Ferdy",
                    LastName="Adamo",
                    Department="Research and Development",
                    Email = "fadamo9@ameblo.jp",
                    StartEmploymentDate = new DateTime(2011, 8, 7)
                },  new Person{
                    Id=5,
                    FirstName="Kerri",
                    LastName="Thams",
                    Department="Human Resources",
                    Email = "kthamsj@qq.com",
                    StartEmploymentDate = new DateTime(2017, 3, 1)
                }
            };
        }
        [Test]
        public void ShouldRetrunPersonByFirstname() {
            var mockDataRetrival = new Mock<IDataRetrival>();
            var mockNotifier = new Mock<INotifier>();

            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);

            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);
            var result = dataManager.GetByFirstName("Kerri");

            Assert.AreEqual(result[0].LastName, "Thams");
        }

        [Test]
        public void ShouldRetrunPersonByLastname()
        {
            var mockDataRetrival = new Mock<IDataRetrival>();
            var mockNotifier = new Mock<INotifier>();
            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);
            var result = dataManager.GetByLastName("Thams");
            Assert.AreEqual(result[0].FirstName, "Kerri");
        }


        [Test]
        public void ShouldReturnAllPeopleFromDepartment()
        {
            var mockDataRetrival = new Mock<IDataRetrival>();
            var mockNotifier = new Mock<INotifier>();
            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);
            var result = dataManager.GetByDepartment("Development");
            Assert.AreEqual(result.Count, 2);
        }

        [Test]
        public void ShouldReturnPersonById()
        {
            Mock<IDataRetrival> mockDataRetrival = new Mock<IDataRetrival>();
            Mock<INotifier> mockNotifier = new Mock<INotifier>();

            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);

            var result = dataManager.GetById(1);

            Assert.AreEqual(result.FirstName, "Ursa");
            Assert.AreEqual(result.LastName, "Maskell");
            Assert.AreEqual(result.Department, "Engineering");
        }

        [Test]
        public void ShouldReturnAllowedWorkFromHome()
        {
            Mock<IDataRetrival> mockDataRetrival = new Mock<IDataRetrival>();
            Mock<INotifier> mockNotifier = new Mock<INotifier>();

            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);

            var result = dataManager.IsAllowedWorkFromHome(1);

            Assert.IsTrue(result);
        }

        [Test]
        public void ShouldReturnNotAllowedWorkFromHome()
        {
            Mock<IDataRetrival> mockDataRetrival = new Mock<IDataRetrival>();
            Mock<INotifier> mockNotifier = new Mock<INotifier>();

            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);

            var result = dataManager.IsAllowedWorkFromHome(5);

            Assert.IsFalse(result);
        }

        [Test]
        public void ShouldReturn23DaysOfVacation()
        {
            Mock<IDataRetrival> mockDataRetrival = new Mock<IDataRetrival>();
            Mock<INotifier> mockNotifier = new Mock<INotifier>();

            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);

            var result = dataManager.GetNumberOfVacationDays(5);

            Assert.IsTrue(result == 23);
        }

        [Test]
        public void ShouldSendNotificationWith26DaysOfVacation()
        {
            Mock<IDataRetrival> mockDataRetrival = new Mock<IDataRetrival>();
            Mock<INotifier> mockNotifier = new Mock<INotifier>();

            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);

            var result = dataManager.GetNumberOfVacationDays(4);
            var message = $"Number of vacation days for {DateTime.Now.Year} is {result}";
            dataManager.Notify(dataManager.GetById(4), message);
            mockNotifier.Verify(x => x.SendNotification("fadamo9@ameblo.jp", message), Times.Once());
        }

        [Test]
        public void ShouldReturn29DaysOfVacation()
        {
            Mock<IDataRetrival> mockDataRetrival = new Mock<IDataRetrival>();
            Mock<INotifier> mockNotifier = new Mock<INotifier>();

            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);

            var result = dataManager.GetNumberOfVacationDays(3);

            Assert.AreEqual(result, 29);
        }


        [Test]
        public void ShouldNotificationBeenSent() {
            var mockDataRetrival = new Mock<IDataRetrival>();
            var mockNotifier = new Mock<INotifier>();
            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);
            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);

            var person = dataManager.GetById(1);
            dataManager.Notify(person, "message");
            mockNotifier.Verify(x => x.SendNotification(It.IsAny<string>(), "message"), Times.Once);
        }

        [Test]
        public void ShouldSendNotificationToAGroup()
        {
            Mock<IDataRetrival> mockDataRetrival = new Mock<IDataRetrival>();
            Mock<INotifier> mockNotifier = new Mock<INotifier>();

            mockDataRetrival.Setup(x => x.GetAll()).Returns(employeeList);

            var dataManager = new DataManager(mockDataRetrival.Object, mockNotifier.Object);

            dataManager.NotifyDepartmant("Engineering");

            mockNotifier.Verify(x => x.SendNotificationToGroup(It.IsAny<List<Person>>(), It.IsAny<string>()), Times.Once());
        }
    }
}
